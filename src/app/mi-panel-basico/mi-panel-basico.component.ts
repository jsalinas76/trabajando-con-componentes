import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'panel-basico',
  templateUrl: './mi-panel-basico.component.html',
  styleUrls: ['./mi-panel-basico.component.css']
})
export class MiPanelBasicoComponent implements OnInit {

  clase: string;
  titulo: string = 'Estoy cargando un título desde el controlador';
  subtitulo: string = 'Estoy cargando un subtítulo desde el controlador';
  parrafo: string = 'Este párrafo ha sido cargado desde el controlador';
  url1: string = 'http://google.es';
  url2: string = 'https://cleformacion.com';
  visible: boolean = true;

  constructor() {
    this.clase= 'panel';
  }

  ngOnInit(): void {
    console.log('Componente correctamente inicilizado ...');
  }

  onclick(evento: Event){
    this.visible = !this.visible;
  }

  ondblclick(header: HTMLElement){
    this.clase = 'panel-amarillo';
  }

}

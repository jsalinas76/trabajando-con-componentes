import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiPanelBasicoComponent } from './mi-panel-basico.component';

describe('MiPanelBasicoComponent', () => {
  let component: MiPanelBasicoComponent;
  let fixture: ComponentFixture<MiPanelBasicoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiPanelBasicoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MiPanelBasicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

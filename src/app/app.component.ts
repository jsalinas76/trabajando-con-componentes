import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'trabajando-con-componentes';
  mensaje = 'Mensaje 1';
  datos = 'Esta información es muy interesante ...';

  recuperarInfo(mensaje: string){
    alert(mensaje);
  }

}

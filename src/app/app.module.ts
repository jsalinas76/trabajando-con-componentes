import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MiPanelBasicoComponent } from './mi-panel-basico/mi-panel-basico.component';
import { MiPanelParametrizadoComponent } from './mi-panel-parametrizado/mi-panel-parametrizado.component';
import { MiAcordeonComponent } from './mi-acordeon/mi-acordeon.component';
import { MiBotonComponent } from './mi-boton/mi-boton.component';
import { PanelActivoComponent } from './panel-activo/panel-activo.component';
import { CampoEmisorComponent } from './campo-emisor/campo-emisor.component';


@NgModule({
  declarations: [
    AppComponent,
    MiPanelBasicoComponent,
    MiPanelParametrizadoComponent,
    MiAcordeonComponent,
    MiBotonComponent,
    PanelActivoComponent,
    CampoEmisorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelActivoComponent } from './panel-activo.component';

describe('PanelActivoComponent', () => {
  let component: PanelActivoComponent;
  let fixture: ComponentFixture<PanelActivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelActivoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelActivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

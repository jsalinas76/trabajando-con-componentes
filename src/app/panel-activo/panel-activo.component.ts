import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'panel-activo',
  templateUrl: './panel-activo.component.html',
  styleUrls: ['./panel-activo.component.css']
})
export class PanelActivoComponent implements OnInit {

  @Input() texto: string = '';
  estilos: string = 'background-color: red;';

  constructor() { }

  ngOnInit(): void {
  }

  cambiarFondoBlue(): void{
    this.estilos = 'background-color: blue;'
  }

  cambiarFondoRed(): void{
    this.estilos = 'background-color: red;'
  }

}

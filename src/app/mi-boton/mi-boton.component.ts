import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mi-boton',
  templateUrl: './mi-boton.component.html',
  styleUrls: ['./mi-boton.component.css']
})
export class MiBotonComponent implements OnInit {


  @Input() texto: string = 'Guardar factura';
  claseCss: string = 'light';

  constructor() { }

  ngOnInit(): void {
  }

  cambiarColorFondo(){
    console.log('se está ejecutando ...');
    if(this.claseCss === 'dark'){
      this.claseCss = 'light';
    }else{
      this.claseCss = 'dark';
    }
  }

}

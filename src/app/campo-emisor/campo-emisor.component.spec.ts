import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampoEmisorComponent } from './campo-emisor.component';

describe('CampoEmisorComponent', () => {
  let component: CampoEmisorComponent;
  let fixture: ComponentFixture<CampoEmisorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampoEmisorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampoEmisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

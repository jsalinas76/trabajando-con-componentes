import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'campo-emisor',
  templateUrl: './campo-emisor.component.html',
  styleUrls: ['./campo-emisor.component.css']
})
export class CampoEmisorComponent implements OnInit {

  @Output()
  propagar = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {
  }

  enviarInformacion(valor: string): void{
    this.propagar.emit(valor);
  }

}

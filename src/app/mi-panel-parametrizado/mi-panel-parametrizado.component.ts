import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mi-panel-parametrizado',
  templateUrl: './mi-panel-parametrizado.component.html',
  styleUrls: ['./mi-panel-parametrizado.component.css'],
})
export class MiPanelParametrizadoComponent implements OnInit {
  @Input() texto: string;
  @Input() titulo: string;

  constructor() {
    this.texto = 'esto es una prueba';
    this.titulo = 'titulo de prueba';
  }

  ngOnInit(): void {}
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiPanelParametrizadoComponent } from './mi-panel-parametrizado.component';

describe('MiPanelParametrizadoComponent', () => {
  let component: MiPanelParametrizadoComponent;
  let fixture: ComponentFixture<MiPanelParametrizadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiPanelParametrizadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MiPanelParametrizadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mi-acordeon',
  templateUrl: './mi-acordeon.component.html',
  styleUrls: ['./mi-acordeon.component.css']
})
export class MiAcordeonComponent implements OnInit {

  @Input() titulo: string = '';
  @Input() contenido: string = '';
  @Input() colorFondo: string = '';

  bgstyle: string = '';
  visible: boolean = true;

  ngOnInit(): void {
    this.bgstyle = `background-color: ${this.colorFondo};`;
  }

  onclick(){
    this.visible = !this.visible;
  }
}
